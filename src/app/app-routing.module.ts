import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ListpqrComponent } from "./pages/listpqr/listpqr.component";
import { ItemComponent } from "./pages/item/item.component";
import { CreateComponent } from "./pages/create/create.component";
import { ListclaimComponent } from "./pages/listclaim/listclaim.component";

const app_routes: Routes = [
  { path: "home", component: CreateComponent },
  { path: "pqrs", component: ListpqrComponent },
  { path: "claim", component: ListclaimComponent },
  { path: "item/:id", component: ItemComponent },
  { path: "**", pathMatch: "full", redirectTo: "home" },
];

@NgModule({
  imports: [RouterModule.forRoot(app_routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
