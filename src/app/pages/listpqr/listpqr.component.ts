import { Component, OnInit } from "@angular/core";
import { PqrService } from "src/app/services/pqr.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2";

@Component({
  selector: "app-listpqr",
  templateUrl: "./listpqr.component.html",
  styleUrls: ["./listpqr.component.css"],
})
export class ListpqrComponent implements OnInit {
  pqrs: any;
  pageActual: number = 1;

  constructor(private pqrservice: PqrService, private router: Router) {}

  ngOnInit() {
    this.loadPqr();
  }

  loadPqr() {
    this.SwalApi();

    this.pqrservice.getPqrs(false).subscribe(
      (resp: any) => {
        if (resp.error) {
          this.SwalOpen("error", resp.errorMessage);
        } else {
          Swal.close();
          this.pqrs = resp.body;
        }
      },
      (err) => {
        this.SwalOpen("error", err.error.error);
      }
    );
  }

  SwalOpen(type, text) {
    Swal.fire({
      type: type,
      text: text,
    });
  }

  SwalApi() {
    Swal.fire({
      allowOutsideClick: false,
      type: "info",
      text: "Espere Por Favor",
    });
    Swal.showLoading();
  }
}
