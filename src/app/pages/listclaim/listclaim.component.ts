import { Component, OnInit } from "@angular/core";
import { PqrService } from "src/app/services/pqr.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2";

@Component({
  selector: "app-listclaim",
  templateUrl: "./listclaim.component.html",
  styleUrls: ["./listclaim.component.css"],
})
export class ListclaimComponent implements OnInit {
  pqrs: any;
  pageActual: number = 1;

  constructor(private pqrservice: PqrService) {}

  ngOnInit() {
    this.loadPqr();
  }

  loadPqr() {
    this.SwalApi();

    this.pqrservice.getPqrs(true).subscribe(
      (resp: any) => {
        if (resp.error) {
          this.SwalOpen("error", resp.errorMessage);
        } else {
          Swal.close();
          this.pqrs = resp.body;
        }
      },
      (err) => {
        this.SwalOpen("error", err.error.error);
      }
    );
  }

  SwalOpen(type, text) {
    Swal.fire({
      type: type,
      text: text,
    });
  }

  SwalApi() {
    Swal.fire({
      allowOutsideClick: false, //saber si se cierra en el fondo
      type: "info",
      text: "Espere Por Favor",
    });
    Swal.showLoading();
  }
}
