import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { PqrModel } from "../../models/pqr.model";
import { PqrTypeModel } from "../../models/pqrtype.model";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import { PqrTypeService } from "src/app/services/pqrtype.service";
import { PqrService } from "src/app/services/pqr.service";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.css"],
})
export class CreateComponent implements OnInit {
  pqr: PqrModel;
  pqrType: any;
  isClaim: boolean = false;

  constructor(
    private typeService: PqrTypeService,
    private pqrService: PqrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.pqr = new PqrModel();
    this.GetTypePqr();
  }

  CreatePqr(form: NgForm) {
    if (!form.valid) return;

    this.SwalApi();
    this.pqr.Description = form.value.description;
    if (this.isClaim) {
      this.pqrService.createClaim(this.pqr).subscribe(
        (resp: any) => {
          if (resp.error) {
            this.SwalOpen("error", resp.errorMessage);
          } else {
            let swalType = "success";

            if (resp.appCode == "g103") swalType = "error";

            this.SwalOpen(swalType, resp.body);
            form.resetForm();
          }
        },
        (err) => {
          this.SwalOpen("error", err.error.error);
        }
      );
    } else {
      this.pqrService.createPqr(this.pqr).subscribe(
        (resp: any) => {
          if (resp.error) {
            this.SwalOpen("error", resp.errorMessage);
          } else {
            this.SwalOpen("success", "Pqr Creada Correctamente!");
            form.resetForm();
          }
        },
        (err) => {
          this.SwalOpen("error", err.error.error);
        }
      );
    }
  }

  GetTypePqr() {
    this.SwalApi();

    this.typeService.getTypePqr().subscribe(
      (resp: any) => {
        this.pqrType = resp.body;
        Swal.close();
      },
      (err) => {
        this.SwalOpen("error", err.error.error);
      }
    );
  }

  validateTypePqr(selected: any) {
    const type = this.pqrType.filter((ty) => {
      return ty._id == selected;
    });
    if (type.length > 0 && !type[0].isMain) {
      this.isClaim = true;
    } else {
      this.isClaim = false;
    }
  }

  SwalOpen(type, text) {
    Swal.fire({
      type: type,
      text: text,
    });
  }

  SwalApi() {
    Swal.fire({
      allowOutsideClick: false, //saber si se cierra en el fondo
      type: "info",
      text: "Espere Por Favor",
    });
    Swal.showLoading();
  }
}
