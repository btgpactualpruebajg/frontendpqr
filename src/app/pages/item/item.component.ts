import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PqrService } from "src/app/services/pqr.service";
import Swal from "sweetalert2";
@Component({
  selector: "app-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.css"],
})
export class ItemComponent implements OnInit {
  pqr: any;

  constructor(
    private route: ActivatedRoute,
    public pqrService: PqrService,
    private redirecionar: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe((parametros) => {
      this.loadPqr(parametros["id"]);
    });
  }

  loadPqr(id: string) {
    this.SwalApi();

    this.pqrService.getPqrId(id).subscribe(
      (resp: any) => {
        if (resp.error) {
          this.SwalOpen("error", resp.errorMessage);
        } else {
          Swal.close();
          this.pqr = resp.body[0];
        }
      },
      (err) => {
        this.SwalOpen("error", err.error.error);
      }
    );
  }

  SwalOpen(type, text) {
    Swal.fire({
      type: type,
      text: text,
    });
  }

  SwalApi() {
    Swal.fire({
      allowOutsideClick: false, //saber si se cierra en el fondo
      type: "info",
      text: "Espere Por Favor",
    });
    Swal.showLoading();
  }
}
