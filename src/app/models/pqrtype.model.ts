export class PqrTypeModel {
  _id: string;
  name: string;
  slug: string;
  active: boolean;
}
