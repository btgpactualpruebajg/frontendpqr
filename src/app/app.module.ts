import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./shared/header/header.component";
import { FooterComponent } from "./shared/footer/footer.component";
import { AppRoutingModule } from "./app-routing.module";
import { NgxPaginationModule } from "ngx-pagination"; // <-- import the module

import { HttpClientModule } from "@angular/common/http";
import { ItemComponent } from "./pages/item/item.component";
import { CreateComponent } from "./pages/create/create.component";
import { ListpqrComponent } from "./pages/listpqr/listpqr.component";
import { ListclaimComponent } from "./pages/listclaim/listclaim.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ItemComponent,
    CreateComponent,
    ListpqrComponent,
    ListclaimComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
