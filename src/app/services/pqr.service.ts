import { Injectable } from "@angular/core";
import { PqrModel } from "../models/pqr.model";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class PqrService {
  private url = environment.urlApi + "/pqr";

  constructor(private http: HttpClient) {}

  createClaim(pqr: PqrModel) {
    const pqrData = {
      idPqrType: pqr.IdPqrType,
      messageUser: pqr.Description,
      settled: pqr.Settled,
    };
    return this.http.put(this.url + "/claim", pqrData).pipe(
      map((resp) => {
        return resp;
      })
    );
  }

  createPqr(pqr: PqrModel) {
    const pqrData = {
      idPqrType: pqr.IdPqrType,
      description: pqr.Description,
    };
    //pipe obtener respuesta
    return this.http.post(this.url + "", pqrData).pipe(
      map((resp) => {
        return resp;
      })
    );
  }

  getPqrs(isClaim: boolean) {
    return this.http.get(this.url + "/" + isClaim).pipe(
      map((resp) => {
        return resp;
      })
    );
  }

  getPqrId(id: string) {
    return this.http.get(this.url + "/1/" + id).pipe(
      map((resp) => {
        return resp;
      })
    );
  }
}
