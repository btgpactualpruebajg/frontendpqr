import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { InfoPagina } from "../interfaces/info-pagina.interface";

@Injectable({
  providedIn: "root",
})
export class InfopaginaService {
  info: InfoPagina = {};
  productos: any = [];
  hayresultadoFiltro = false;
  productosFiltrado = [];

  constructor(private http: HttpClient) {
    this.cargarInfo();
  }

  private cargarInfo() {
    //Leer archivo json
    this.http
      .get("assets/data/data-pagina.json")
      .subscribe((resp: InfoPagina) => {
        this.info = resp;
      });
  }
}
