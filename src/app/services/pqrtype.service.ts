import { Injectable } from "@angular/core";
import { PqrModel } from "../models/pqr.model";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class PqrTypeService {
  private url = environment.urlApi + "/pqrtype";

  constructor(private http: HttpClient) {}

  getTypePqr() {
    return this.http.get(this.url).pipe(
      map((resp) => {
        return resp;
      })
    );
  }
}
